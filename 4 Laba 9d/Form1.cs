﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _4_Laba_9d
{
	public partial class Form1 : Form
	{
		public int[] array;

		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			Random random = new Random();
			array = new int[(int)countElements.Value];
			array = array.Select(x => random.Next((int)minLimit.Value, (int)(maxLimit.Value + 1))).ToArray();
			int k = 0;
			for (int j = 1; array.Length > (Math.Pow(2, j) - Math.Pow(-1, j)) / 3; j++)
			{
				k = j;
			}
			var d = (Math.Pow(2, k) - Math.Pow(-1, k)) / 3;
			while (d >= 1)
			{
				for (var i = d; i < array.Length; i++)
				{
					var j = i;
					while ((j >= d) && (array[(int)(j - d)] > array[(int)j]))
					{
						Swap(ref array[(int)j], ref array[(int)(j - d)]);
						j = j - d;
					}
					drawShellSort(array, -1);
					drawMarking();
					Thread.Sleep(200);
				}
				k--;
				d = (Math.Pow(2, k) - Math.Pow(-1, k)) / 3;
			}
		}

		private void Swap(ref int a, ref int b)
		{
			var t = a;
			a = b;
			b = t;
		}

		private void drawShellSort(int[] array, int a)
		{
			bool ok = true;
			Pen pen = new Pen(Color.DarkOrange);

			Graphics graphics = pictureBox1.CreateGraphics();
			graphics.Clear(Color.Black);
			for (int i = (int)minLimit.Value; i <= maxLimit.Value; i++)
			{
				for (int j = 0; j < array.Length; j++)
				{
					{
						if (ok)
							pen = new Pen(Color.Blue);
						else
							pen = new Pen(Color.DarkOrange);
						ok = !ok;
					}
					if (array[j] >= i)
						graphics.FillRectangle(new SolidBrush(pen.Color), 12 * j, pictureBox1.Height - 12 * i, 12, 12);
				}
			}
		}

		private void drawMarking()
		{
			Graphics graphics = pictureBox1.CreateGraphics();

			Pen pen = new Pen(Color.DarkGreen);
			for (int i = 0; i < pictureBox1.Height; i += 12)
				graphics.DrawLine(pen, 0, pictureBox1.Height - i, pictureBox1.Width, pictureBox1.Height - i);
			for (int i = 0; i < pictureBox1.Width; i += 12)
				graphics.DrawLine(pen, i, 0, i, pictureBox1.Width);
		}

		private void pictureBox1_Paint(object sender, PaintEventArgs e)
		{
			((PictureBox)sender).CreateGraphics().Clear(Color.Black);
			drawMarking();
		}
	}
}


